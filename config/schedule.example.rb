ROOT_PATH =  File.expand_path('..', File.dirname(__FILE__))
env :PATH, ENV['PATH']

every 1.day, :at => '1:00 am' do
  command "#{ROOT_PATH}/get_appointments.rb --upload"
end
