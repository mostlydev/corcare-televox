module Televox
  HOST = 'ssh3.my.televox.com'
  USER = 'someuser'
  PASSWORD = 'somepass'
  PATH = '.'
  START_DAYS_OUT = 4
  END_DAYS_OUT = 12
end