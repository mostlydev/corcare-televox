require 'net/http'
require 'json'


class Amf
    def initialize(uri)
      @base_uri = uri
    end

    def post(service_and_function, params = {} )
      uri = URI.parse(@base_uri + service_and_function)
 
      response = Net::HTTP::post_form( uri, params )
      JSON.parse(response.body)
    end
end