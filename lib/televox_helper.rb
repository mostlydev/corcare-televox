require 'net/sftp'

class TelevoxHelper
  attr_reader :csv_string, :apps, :clinicians

  def initialize(amf_uri)
    @amf = Amf.new amf_uri
  end

  def upload_csv_to( host, user, password, path )
    result = nil
    timestamp = Time.now.to_i.to_s
    tmp_file = "/tmp/#{timestamp}.phonedata.csv"
    File.open(tmp_file, 'w') { |f| f.write(@csv_string) }
    Net::SFTP.start( host, user, :password => password ) do |sftp|
      result = sftp.upload!( tmp_file, "#{path}/Appt.csv" )
    end
    result
  end

  def get_appointments( start_at, end_at )
    @apps = @amf.post( 'Appointments.getBetweenDates',
                     { 'startDate' => Date.today + start_at ,
                       'endDate' => Date.today + end_at })
    @apps
  end

  def get_clinicians
    @clinicians = []
    @apps.map{|a| a['clinID'] }.uniq.each { | clin_id |
      clin_id_params = { 'clinId' => clin_id.to_s }
      @clinicians << @amf.post( 'Clinicians.getById', clin_id_params )
    }
  end

  def make_csv
    @csv_string = CSV.generate do |csv|
      csv << %w( 'appointment_id', 'procedure', 'appointment_date', 'appointment_time',
                      'app_cancelled', 'app_no_show', 'app_confirmed', , 'patient_id', 'patient_first_name', 'patient_last_name'
                      'birth_date', 'phone', 'cell_phone', 'business_phone',
                      'postal_code', 'province', 'location_name', 'doctor_id',
                      'doctor_first_name', 'doctor_last_name' )

      @apps.each do |app|
        cor_md = @clinicians.find{|clin| clin['id'].to_s == app['clinID'] }
        new_item = [
            app['appID'],
            app['bookLabel'],
            app['appTime'].to_s[0..9],
            app['appTime'].to_s[11..18],
            app['appIsCancelled'],
            app['appIsNoShow'],
            app['appConfirmed'],
            app['patID'],
            app['patFirstName'],
            app['patLastName'],
            app['patBirthdate'],
            app['patPhone'].gsub(/^\($/,''),
            app['patCellPhone'].gsub(/^\($/,''),
            app['patBusinessPhone'].gsub(/^\($/,''),
            app['patPostalCode'],
            app['patProvince'],
            app['locName'].to_s,
            cor_md['id'],
            cor_md['firstName'].to_s,
            cor_md['lastName'].to_s
        ]

        csv << new_item
      end
    end
  end
end
