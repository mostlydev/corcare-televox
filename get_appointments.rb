#!/usr/bin/env ruby

require 'date'
require 'csv'
require_relative 'config/environment'


televox = TelevoxHelper.new( CorCare::AMF_URI)

print 'Getting appointments... '
televox.get_appointments( Televox::START_DAYS_OUT, Televox::END_DAYS_OUT )
puts "#{televox.apps.count} found"

print 'Pulling clinician list... '
televox.get_clinicians
puts "#{televox.clinicians.count} found"

print 'Making CSV... '
televox.make_csv
puts 'OK'

if ARGV.include?('--print')
  puts televox.csv_string
elsif ARGV.include?('--upload')
  print "Uploading to #{Televox::HOST}:#{Televox::PATH}... "
  televox.upload_csv_to Televox::HOST, Televox::USER, Televox::PASSWORD, Televox::PATH
  puts "OK"
else
  puts 'Output not requested, try adding `--print` or `--upload` to your command.'
end
