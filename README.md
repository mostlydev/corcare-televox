Author: Andrew Bringaze <abringaze@pinncomp.com>
Author: Wojtek Grabski <wojtek@fluxinc.ca>
Date: 2014-09-28  


# Purpose

These will be or are a series of scripts that pull appointment details
from the Corcare helpers database, and output the data to CSV; as well 
as upload it to a Televox SFTP server.

## Installation

1. Pull from the repository
2. Copy `config/corcare.example.rb` to `config/corcare.rb` and edit to taste.
3. Copy `config/televox.example.rb` to `config/televox.rb` and edit to spec.
4. Copy `config/schedule.example.rb` into place and edit to desired schedule.
5. Run `bundle install` to install the necessary gems.
6. Run `whenever -w` to write the schedule to the current user's crontab.


